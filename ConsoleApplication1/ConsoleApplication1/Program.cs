﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace UDPClient
{
    class Program
    {
        static UdpClient udpc;
        //static UdpClient receivingClient;
        static Thread receivingThread;

        private void InitializeReceiver()
        {
            ThreadStart start = new ThreadStart(Receiver);
            receivingThread = new Thread(start);
            receivingThread.IsBackground = true;

            receivingThread.Start();

        }

        private void Receiver()
        {
            IPEndPoint endPoint = new IPEndPoint(IPAddress.Any, 2055);


            while (true)
            {
                byte[] data = udpc.Receive(ref endPoint);
                string message = Encoding.ASCII.GetString(data);
                Console.WriteLine("{0}", message);
            }
        }


        static void Main(string[] args)
        {

            Program program = new Program();
            udpc = new UdpClient("127.0.0.1", 2055);
            List<byte> data = new List<byte>();
            program.InitializeReceiver();
            String text = "";
            Console.WriteLine("Enter Process id");
            int id = Convert.ToInt32(Console.ReadLine());
            
            Console.WriteLine("Enter Message");
            string Name = Console.ReadLine();
            
            data.AddRange(Encoding.ASCII.GetBytes(Name));
            udpc.Send(data.ToArray(), data.Count);
            udpc.EnableBroadcast = true;

            while (true)
            {

                data.Clear();
                text = Console.ReadLine();
                int textLength = System.Text.ASCIIEncoding.ASCII.GetByteCount(text);
                data.AddRange(Encoding.ASCII.GetBytes(text));
                udpc.Send(data.ToArray(), data.Count);

            }

        }







    }
}
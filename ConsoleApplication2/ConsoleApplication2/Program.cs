﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.Configuration;

namespace ClientServerModel
{
    class Program
    {
        static void Main(string[] args)
        {
            UdpClient udpc = new UdpClient(2055);
            Console.WriteLine("Server started, servicing on port 2055");
            IPEndPoint ep = new IPEndPoint(IPAddress.Broadcast, 2055); ;
            while (true)
            {
                byte[] rdata = udpc.Receive(ref ep);
                List<byte> by = new List<byte>();
                by.AddRange(rdata);
                int id = BitConverter.ToInt32(by.ToArray(), 0);
                by.RemoveRange(0, 4);
                
                string Message = Encoding.ASCII.GetString(by.ToArray());
                Console.WriteLine("Received" + id + "\t" + Message);
                var ResponseData = Encoding.ASCII.GetBytes(id + "\t" + Message);
                udpc.Send(ResponseData, ResponseData.Length, ep);

                by.Clear();

            }
        }
    }
}